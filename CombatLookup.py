import requests

class combatlookup:

	def get_rsn(self, rsn):
		rsurl = "http://services.runescape.com/m=hiscore_oldschool/index_lite.ws?player="
		skillname = ["overall", "attack", "defense", "strength", "hitpoints",
        	"ranged", "prayer", "magic", "cooking", "woodcutting", "fletching",
        	"fishing", "firemaking", "crafting", "smithing", "mining", "herblore",
        	"agility", "thieving", "slayer", "farming", "runecrafting", "hunter", 
        	"construction"]

		self.username = rsn.replace(" ", "_")
		hiscorerequest = requests.get(rsurl+self.username)

		if hiscorerequest.status_code == 200:
			self.hiscore = zip(skillname, hiscorerequest.text.split("\n"))
			return self.hiscore
		else:
			return None

	def get_attack(self):
		return int(self.hiscore[1][1].split(",")[1])
	def get_defense(self):
		return int(self.hiscore[2][1].split(",")[1])
	def get_strength(self):
		return int(self.hiscore[3][1].split(",")[1])
	def get_hp(self):
		return int(self.hiscore[4][1].split(",")[1])