import web
from web import form
from CombatLookup import combatlookup

render = web.template.render('templates/')

urls = ('/' , 'index')
app = web.application(urls, globals())
statlookup = combatlookup() #calls CombatLookup.py

myform = form.Form(
	form.Textbox("Player1", #for testing use zezima or else code will fail
		maxlength="12",
		description="Runescape Name"),
	form.Textbox("Player2", 
		maxlength="12"),
	form.Textbox("Attack"),
	form.Textbox("Attack2"),
	form.Textbox("Attack Bonus", #everything below is required
		form.notnull,
		maxlength="3"),
	form.Textbox("Attack Bonus2",
		form.notnull,
		maxlength="3"),
	form.Textbox("Strength Bonus",
		form.notnull,
		maxlength="3"),
	form.Textbox("Strength Bonus2", 
		form.notnull,
		maxlength="3"),
	form.Checkbox("DDS"),
	form.Dropdown('Attack Style', ['Accurate', 'Aggressive', 'Defensive', 'Controlled'])
	)

class index:

	def GET(self):
		form = myform()
		return render.anotherform(form)
		
	def POST(self):
		form = myform()

		if not form.validates():
			return render.anotherform(form)

		else:
			statlookup.get_rsn(form['Player1'].value) #Sets the value that is sent to get_rsn to the form value of Player1
			form['Attack'].value = statlookup.get_attack() #Takes value and sets that for value to what was pulled
			#need a better way of populating the fields 
			return render.anotherform(form)

if __name__=="__main__":
	web.internalerror = web.debugerror
	app.run()
