import random
from math import *
from CombatLookup import combatlookup

def randint(a,b):
    return a+random.random()*(b-a)

class player:
	victories = 0

	def set_levels(self, attack, strength, defense):
		self.attack_level = attack
		self.strength_level = strength
		self.defense_level = defense
		

	def set_opponent_levels(self, attack, strength, defense):
		self.opponent_attack_level 			= attack
		self.opponent_strength_level 		= strength
		self.opponent_defense_level 		= defense
		

	def set_bonuses(self, attack, strength, defense):
		self.attack_bonus 	= attack
		self.strength_bonus = strength
		self.defense_bonus 	= defense
		

	def set_multiplier(self, mult):
		self.multiplier = mult
		

	def set_name(self, playername):
		self.name = playername
		

	def set_style(self, attackstyle):
		if attackstyle == "a":
			self.attack_style 	= 3
			self.strength_style = 0
			self.defense_style 	= 0
		elif attackstyle == "s":
			self.attack_style 	= 0
			self.strength_style = 3
			self.defense_style 	= 0
		elif attackstyle == "d":
			self.attack_style 	= 0
			self.strength_style = 0
			self.defense_style 	= 3
		elif attackstyle == "c":
			self.attack_style 	= 1
			self.strength_style = 1
			self.defense_style 	= 1
		else:
			return 1

		

	def set_hitpoints(self, hp):
		self.hitpoints = hp
		self.remaining_hitpoints = hp
		

	def max_rolls(self):
		self.ea = self.attack_level + self.attack_style + 8
		self.mar = self.ea * (1 + self.attack_bonus/64)
		self.max_attack_roll = self.mar

		self.es = self.strength_level + self.strength_style + 8
		self.msr = self.es * (1 + self.strength_bonus/64)
		self.max_strength_roll = self.msr

		self.ed = self.defense_level + self.defense_style + 8
		self.mdr = self.ed * (1 + self.defense_bonus/64)
		self.max_defense_roll = self.mdr 

		

	def roll(self):
		self.attack_roll 	= floor(random.randint(0, self.max_attack_roll) / 10)
		self.strength_roll 	= floor(random.randint(0, self.max_strength_roll) / 10)
		self.defense_roll 	= floor(random.randint(0, self.max_defense_roll) / 10)

	def attack(self, defense):
		if self.attack_roll > defense:
			self.damage = self.strength_roll
		else:
			self.damage = 0
			

player1 = player()
player2 = player()
swag 	= combatlookup()

iterations = 100000

player1.set_name("fujitsu25")
swag.get_rsn(player1.name)
player1.set_levels(swag.get_attack(), swag.get_strength(), swag.get_defense())
player1.set_bonuses(82,82,0)
player1.set_style("c")
player1.set_hitpoints(swag.get_hp())

player2.set_name("i gwd baked")
swag.get_rsn(player2.name)
player2.set_levels(swag.get_attack(), swag.get_strength(), swag.get_defense())
player2.set_bonuses(82,82,0)
player2.set_style("c")
player2.set_hitpoints(swag.get_hp())

player1.set_opponent_levels(player2.attack_level, player2.strength_level, player2.defense_level)
player2.set_opponent_levels(player1.attack_level, player1.strength_level, player2.defense_level)

player1.max_rolls()
player2.max_rolls()
dead = False

for i in range(iterations/2):
	while not dead:
		player1.roll()
		player2.roll()

		player1.attack(player2.defense_roll)
		player2.remaining_hitpoints -= player1.damage

		if player2.remaining_hitpoints <= 0:
			dead = True
			player1.victories += 1

		else:
			player2.attack(player1.defense_roll)
			player1.remaining_hitpoints -= player2.damage
			if player1.remaining_hitpoints <= 0:
				dead = True
				player2.victories += 1
	player1.remaining_hitpoints = player1.hitpoints
	player2.remaining_hitpoints = player2.hitpoints
	dead = False

for i in range(iterations/2):
	while not dead:
		player1.roll()
		player2.roll()

		player2.attack(player1.defense_roll)
		player1.remaining_hitpoints -= player2.damage

		if player1.remaining_hitpoints <= 0:
			dead = True
			player2.victories += 1

		else:
			player1.attack(player2.defense_roll)
			player2.remaining_hitpoints -= player1.damage
			if player2.remaining_hitpoints <= 0:
				dead = True
				player1.victories += 1
	player1.remaining_hitpoints = player1.hitpoints
	player2.remaining_hitpoints = player2.hitpoints
	dead = False

print "%s : %d" % (player1.name, player1.victories)
print "%s : %d" % (player2.name, player2.victories)